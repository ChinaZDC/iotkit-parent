/*
 *
 *  * | Licensed 未经许可不能去掉「OPENIITA」相关版权
 *  * +----------------------------------------------------------------------
 *  * | Author: xw2sy@163.com
 *  * +----------------------------------------------------------------------
 *
 *  Copyright [2024] [OPENIITA]
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * /
 */

package cc.iotkit.manager.dto.bo.product;

import cc.iotkit.common.api.BaseDto;
import cc.iotkit.model.product.Icon;
import io.github.linpeilie.annotations.AutoMapper;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;


@ApiModel(value = "IconBo")
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = Icon.class, reverseConvertGenerate = false)
public class IconBo extends BaseDto {

    private static final long serialVersionUID = -1L;

    @ApiModelProperty(value = "id")
    private Long id;

    @ApiModelProperty(value = "图标分类id")
    private Long iconTypeId;

    @ApiModelProperty(value = "图标名称")
    private String iconName;

    @ApiModelProperty(value = "视窗缩放")
    private String viewBox;

    @ApiModelProperty(value = "命名空间")
    private String xmlns;

    @ApiModelProperty(value = "版本")
    private String version;

    @Size(max = 65535, message = "图标内容长度不正确")
    @ApiModelProperty(value = "图标内容")
    private String iconContent;

}
