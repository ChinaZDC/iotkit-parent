/*
 *
 *  * | Licensed 未经许可不能去掉「OPENIITA」相关版权
 *  * +----------------------------------------------------------------------
 *  * | Author: xw2sy@163.com
 *  * +----------------------------------------------------------------------
 *
 *  Copyright [2024] [OPENIITA]
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * /
 */

package cc.iotkit.manager.dto.bo.product;

import cc.iotkit.common.api.BaseDto;
import cc.iotkit.common.validate.AddGroup;
import cc.iotkit.common.validate.EditGroup;
import cc.iotkit.model.product.Product;
import io.github.linpeilie.annotations.AutoMapper;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Min;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;


@ApiModel(value = "ProductBo")
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = Product.class, reverseConvertGenerate = false)
public class ProductBo extends BaseDto {

    private static final long serialVersionUID = -1L;

    @ApiModelProperty(value = "id")
    private Long id;
    @ApiModelProperty(value = "productKey")
    private String productKey;

    @ApiModelProperty(value = "品类")
    @Size(max = 255, message = "品类长度不正确")
    private String category;

    @ApiModelProperty(value = "创建时间")
    private Long createAt;

    @ApiModelProperty(value = "图片")
    @Size(max = 255, message = "图片长度不正确")
    private String img;

    @ApiModelProperty(value = "产品图标ID")
    private Long iconId;

    @ApiModelProperty(value = "产品名称")
    @Size(max = 255, message = "产品名称长度不正确")
    private String name;

    @ApiModelProperty(value = "节点类型")
    private Integer nodeType;

    @ApiModelProperty(value = "是否透传,true/false")
    private Boolean transparent;

    @ApiModelProperty(value = "是否开启设备定位,true/false")
    private Boolean isOpenLocate;

    @ApiModelProperty(value = "定位更新方式")
    private String locateUpdateType;

    @ApiModelProperty(value = "用户ID")
    @Size(max = 255, message = "用户ID长度不正确")
    private String uid;

    @ApiModelProperty(value = "产品密钥")
    @Size(max = 255, message = "产品密钥长度不正确")
    private String productSecret;

    @ApiModelProperty(value = "保活时长")
    @NotNull(message = "保活时长不能为空",groups = { AddGroup.class, EditGroup.class })
    private Long keepAliveTime;

}
